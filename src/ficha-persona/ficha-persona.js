import { LitElement, html, css } from 'lit-element';
import {OrigenPersona} from '../origen-persona/origen-persona';

class FichaPersona  extends LitElement {

  static get styles() {
    return css`
      div{
          border: 1px solid;
          border-radius: 10px;
          padding: 10px;
          margin: 10px;
      }
    `;
  }

  static get properties() {
    return {
        nombre: {type: String},
        apellido: {type: String},
        aniosAnti: {type: Number},
        foto: {type: Object},
        nivel: {type: String},
        bg: {type: String}
    };
  }

  constructor() {
    super();
    this.nombre = "Pedro";
    this.apellido = "Lopez Gonzalez";
    this.aniosAnti = 4;
    this.foto = {
        src : "./src/ficha-persona/img/persona.jpeg",
        alt : "Foto persona"
    };
    this.bg = "aliceblue";
  }

  render() {
    return html`
      <div style="width:400px; background-color:${this.bg}">
        <label for="iNombre">Nombre</label>
        <input type="text" id="iNombre" name="iNombre" value="${this.nombre}" @input="${this.updateNombre}"/>
        <br/>
        <label for="iApellidos">Apellidos</label>
        <input type="text" id="iApellidos" name="iApellidos" value="${this.apellido}"/>
        <br/>
        <label for="iAntiguedad">Antiguedad</label>
        <input type="number" id="iAntiguedad" name="iAntiguedad" value="${this.aniosAnti}" @input="${this.updateAntiguedad}"/>
        <br/>
        <label for="iNivel">Nivel</label>
        <input type="text" id="iNivel" name="iNivel" value="${this.nivel}" disabled/>
        <br/>
        <origen-persona @origen-set="${this.origenChange}"></origen-persona>
        <br/>
        <img src="${this.foto.src}" height="200" width="200" alt="${this.foto.alt}"/>
      </div>
    `;
  }

  updated(changedProperties){
    if(changedProperties.has("nombre")){
      console.log("Propiedad nombre cambiada. Valor anterior: " + changedProperties.get("nombre") + ", valor nuevo: " + this.nombre);
    }
    if(changedProperties.has("aniosAnti")){
      this.actualizarNivel();
    }
  }

  updateNombre(e){
    this.nombre = e.target.value;
  }

  updateAntiguedad(e){
    this.aniosAnti = e.target.value;
  }

  actualizarNivel(){
    if(this.aniosAnti >= 7){
      this.nivel = "Leader";
    }
    else if(this.aniosAnti >= 5){
      this.nivel = "Senior";
    }
    else if(this.aniosAnti >= 3){
      this.nivel = "Team";
    }else{
      this.nivel = "Junior";
    }
  }

  origenChange(e){
    var origen = e.detail.message;
    if(origen === "USA"){
      this.bg = "pink";
    }
    else if (origen === "Mexico"){
      this.bg = "lightgreen";
    }
    else if (origen === "Canada"){
      this.bg = "lightred";
    }
  }

}

customElements.define('ficha-persona', FichaPersona);